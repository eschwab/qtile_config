# Qtile Configuration

Configuration file and extras for qtile, the tiling window manager.

## Files
---------

### config.py
Main configuration file.

### snowjob.py
Fun little script to make it snow in the terminal. Would like to add this as a callable screensaver eventually.
