from os import system
from platform import node
import sh

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook


myyellow = 'ffcb48'
myblue = '98c2c7'
myorange = 'dd5608'

widget_defaults = dict(font='Monospace',
                       fontsize=12)

def get_laptop_bar():
    bar_frogfoot = bar.Bar([
        widget.Battery(
            low_foreground=myorange,
            foreground=myyellow,
            charge_char='+',
            discharge_char='-',
            battery_name=0,
            energy_now_file='charge_now',
            energy_full_file='charge_full',
            power_now_file='current_now',
        ),
        widget.GroupBox(urgent_alert_method='text',
                        borderwidth=1,
                        padding=3,
                        this_current_screen_border=myorange,
                    ),
        widget.WindowName(),
        widget.Net(update_interval=5, foreground=myorange, interface='eno1'),
        widget.Net(update_interval=5, foreground=myorange, interface='wlp2s0'),
        widget.Systray(),
        widget.Clock(timezone='UTC', foreground=myblue),
        widget.Clock(format='%a %F', foreground=myyellow),
        widget.Clock(foreground=myorange),
    ], size=26, background="#202020")
    return bar_frogfoot

def get_badger_bar():
    bar_badger = bar.Bar([
        widget.GroupBox(urgent_alert_method='text',
                        borderwidth=1,
                        padding=3,
                        this_current_screen_border=myorange,
                    ),
        widget.WindowName(),
        widget.Systray(),
        widget.Clock(timezone='UTC', foreground=myblue),
        widget.Clock(format='%a %F', foreground=myyellow),
        widget.Clock(foreground=myorange),
    ], size=26, background="#202020")
    return bar_badger

if node() in ['frogfoot', 'firebird']:
    screens = [Screen(top=get_laptop_bar())]
else:
    screens = [
        Screen(top=get_badger_bar()
        ),
        Screen(top=bar.Bar([
            widget.GroupBox(urgent_alert_method='text',
                            borderwidth=1,
                            padding=3,
                            this_current_screen_border=myorange,),
            widget.CurrentScreen(
                active_text='***',
                active_color=myorange,
                inactive_text=None,),
            widget.WindowName()
            ], size=26, background="#202020"),
        )
    ]

class Commands(object):
    terminal = 'urxvtc'
    calculator = 'galculator'
    dmenu = 'dmenu_run -b -p ">" -sb "#000000" -sf "#dd0808" -nf "#dd5608" \
    -fn "-*-dejavu sans mono-*-r-normal-*-12h-*-*-*-*-*-*-*"'
    Music_Play = 'mpc toggle'
    Music_Stop = 'mpc stop'
    Music_Next = 'mpc next'
    Music_Prev = 'mpc prev'
    Vol_Up = 'amixer -q set Master 3%+'
    Vol_Down = 'amixer -q set Master 3%-'
    Vol_Mute = 'amixer -q set Master toggle'
    Backlight_Up = 'xbacklight -inc 10'
    Backlight_Down = 'xbacklight -dec 10'

mod = "mod4"

keys = [
    Key(["shift", "mod1"], "y",  lazy.shutdown()),
    Key([mod, "shift"], "r",     lazy.restart()),
    Key([mod], "x",              lazy.window.kill()),
    Key([mod], "g",              lazy.switchgroup()),

    # Switching between stacks and clients within a stack.
    Key([mod], "n",              lazy.layout.down()),
    Key([mod], "e",              lazy.layout.up()),
    Key([mod], "h",              lazy.layout.previous()),
    Key([mod], "i",              lazy.layout.next()),

    # Change the order of stacks. Esp for vertical split stacks.
    Key([mod], "m",              lazy.layout.shuffle_up()),
    Key([mod], "l",              lazy.layout.shuffle_down()),

    # Move clients to different stacks.
    Key([mod, "shift"], "h",     lazy.layout.client_to_previous()),
    Key([mod, "shift"], "i",     lazy.layout.client_to_next()),

    # Stack manipulation.
    Key([mod, "shift"], "space", lazy.layout.rotate()),
    Key([mod, "shift"], "Return",lazy.layout.toggle_split()),
    Key(["mod1"], "Tab",         lazy.next_layout()),

    Key([mod], "w",              lazy.window.toggle_fullscreen()),

    # start specific apps
    Key([mod], "space",          lazy.spawn(Commands.dmenu)),
    Key([mod], "Return",         lazy.spawn(Commands.terminal)),
    Key([], "XF86Calculator",    lazy.spawn(Commands.calculator)),

    # Media keys.
    Key([], "XF86AudioRaiseVolume",   lazy.spawn(Commands.Vol_Up)),
    Key([], "XF86AudioLowerVolume",   lazy.spawn(Commands.Vol_Down)),
    Key([], "XF86AudioMute",       lazy.spawn(Commands.Vol_Mute)),
    Key([], "XF86AudioPlay",       lazy.spawn(Commands.Music_Play)),
    Key([], "XF86AudioStop",       lazy.spawn(Commands.Music_Stop)),
    Key([], "XF86AudioNext",       lazy.spawn(Commands.Music_Next)),
    Key([], "XF86AudioPrev",       lazy.spawn(Commands.Music_Prev)),

    Key([], "XF86MonBrightnessUp",       lazy.spawn(Commands.Backlight_Up)),
    Key([], "XF86MonBrightnessDown",       lazy.spawn(Commands.Backlight_Down)),
]

follow_mouse_focus = False
# This allows you to drag windows around with the mouse if you want.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

# Set up badger for two screens
if node() in ['frogfoot', 'foxhound']:
    groups = [
        Group('a', spawn='urxvtc'),
        Group('r', layout='max'),
        Group('s', spawn='urxvtc'),
        Group('t'),
    ]

    for i in groups:
        keys.append(
            Key([mod], i.name, lazy.group[i.name].toscreen(toggle=False))
        )
        keys.append(
            Key([mod, "mod1"], i.name, lazy.window.togroup(i.name))
        )
else:
    groups = []
    # group 'd' is always on screen 1
    for s, i in [(0, "a"), (0, "r"), (0, "s"), (0, "t"), (1, "d")]:
        groups.append(Group(i))
        keys.append(
            Key([mod], i, lazy.group[i].toscreen(s, toggle=False), lazy.to_screen(s)
            )
        )
        keys.append(
            Key([mod, "mod1"], i, lazy.window.togroup(i))
        )

layouts = [
    layout.Stack(stacks=2, border_width=1),
    layout.Max(),
]

@hook.subscribe.startup_once
def wallpaper():
    system("feh --bg-scale ~/Pictures/Wallpaper/Carbon_by_Alexander_GG.jpg")

@hook.subscribe.startup_once
def runner():
    sh.numlockx()
    sh.xrdb('-load', '~/.Xresources')
    if node() == 'badger':
        sh.setxkbmap('us', '-variant', 'colemak')
        #sh.xset('m', '1/1', '4')
        sh.xinput('--set-prop', '11', '298', '-1')
    elif node() == 'frogfoot':
        sh.nm-applet()
    elif node() == 'foxhound':
        sh.setxkbmap('us', '-variant', 'colemak')

@hook.subscribe.client_new
def dialogs(window):
    if(window.window.get_wm_type() == 'dialog'
        or window.window.get_wm_transient_for()):
        window.floating = True

def main(qtile):
    qtile.critical()        # Set logging level. Levels in manager.py

